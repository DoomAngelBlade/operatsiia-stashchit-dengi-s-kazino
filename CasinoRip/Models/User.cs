﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CasinoRip.Models
{
    class User
    {
        public string UserLogin { get; set; }
        public string UserPass { get; set; }
        public string SekretKey { get; set; }
        public string ZendMainLocathion { get; set; }
        public string UserProxy { get; set; }
        public string UserChromePath { get; set; }
        public int Id { get; set; }
    }
}
