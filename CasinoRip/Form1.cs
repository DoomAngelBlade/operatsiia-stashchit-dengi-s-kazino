﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using CasinoRip.Models;
using System.IO;
using System.Threading;
using SteamAuth;
using Anticaptcha_example.Api;
using CasinoRip.Helper;
using System.Globalization;
using System.Diagnostics;

namespace CasinoRip
{
    public partial class Form1 : Form
    {

        private static object Locker = new object();
        List<User> SteamUserList;
        static string AppCurrentDirectory = Directory.GetCurrentDirectory() + "\\";
        private BindingSource bindingSource_users = new BindingSource();
        ConfigForm ConfigForm { get; set; }
        enum SteamLoginCodes
        {
            FailedToLogin = 0,
            LoggedInSteam = 1,
            ChromeNonReach = 2,
            ChromeError = 3,
            AlreadyLogged = 4,
        }
        enum CsgoatseCodes
        {

            CoinsCollected = 0,
            CantCollectCoins = 1,
            ChromeError = 2,
            NotTimeYet = 3,
        }
        private SteamLoginCodes SteamLogin(IWebDriver webDriver, string login, string pass, string sakretKey)
        {

            try
            {
                webDriver.Navigate().GoToUrl("https://steamcommunity.com/login/home/?goto=");
                if (Helper.WFETA.IsElementExists(webDriver, By.ClassName("profile_header_actions"), 40)) { return SteamLoginCodes.AlreadyLogged; } //logged in 
                IWebElement elementUserName = webDriver.FindElement(By.Name("username"));
                elementUserName.SendKeys(login);
                IWebElement elementPassword = webDriver.FindElement(By.Name("password"));
                elementPassword.SendKeys(pass);
                IWebElement elementLogin = webDriver.FindElement(By.Id("SteamLogin"));
                elementLogin.Click();

                Helper.WFETA.IsElementExists(webDriver, By.Id("twofactorcode_entry"), 40);
                IWebElement elementValidateCode = webDriver.FindElement(By.Id("twofactorcode_entry"));
                elementValidateCode.SendKeys(Helper.Helper.GenerateSteamGuardCodeForTime(TimeAligner.GetSteamTime(), sakretKey));
                Thread.Sleep(1000);
                elementValidateCode.SendKeys(OpenQA.Selenium.Keys.Enter);

                if (Helper.WFETA.IsElementExists(webDriver, By.ClassName("profile_header_actions"), 90))
                {
                    return SteamLoginCodes.LoggedInSteam; //logged in 
                }
                else
                {
                    return SteamLoginCodes.FailedToLogin; // Failed to log in, unknown error.
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("chrome not reachable") || ex.Message.Contains("target window already closed"))
                {
                    return SteamLoginCodes.ChromeNonReach; //Failed to log in, chrome not reachable/closed
                }
                else
                {
                    return SteamLoginCodes.ChromeError; // Failed to log in, unknown chrome error.
                }
            }
        }

        #region Сайти
        private CsgoatseCodes CsgoAtse(IWebDriver webDriver, string userLogin)
        {
            int countTrysEnterCaptcha = 4;
            try
            {
                webDriver.Navigate().GoToUrl("https://csgoatse.com/");
                IWebElement roulette = Helper.WFETA.FindElementBySomeTime(webDriver, By.Id("roulette"), 50);
                if (roulette != null)
                {
                    goto Alreadyloggedin;
                }

                webDriver.Navigate().GoToUrl("https://steamcommunity.com/openid/login?openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.mode=checkid_setup&openid.return_to=https%3A%2F%2Fskin2coins.com%2Flogin&openid.realm=https%3A%2F%2Fskin2coins.com&openid.ns.sreg=http%3A%2F%2Fopenid.net%2Fextensions%2Fsreg%2F1.1&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select");
                IWebElement imageLogin = Helper.WFETA.FindElementBySomeTime(webDriver, By.Id("imageLogin"), 50);
                if (imageLogin != null)
                {
                    imageLogin.Click();
                }
                Thread.Sleep(1500);
                Alreadyloggedin:
                webDriver.Navigate().GoToUrl("https://csgoatse.com/");
                IWebElement btn_accept = Helper.WFETA.FindElementBySomeTime(webDriver, By.CssSelector("#tos > div.scroll-wrapper.scrollbar > div.scrollbar.scroll-content.scroll-scrolly_visible > div > a"), 40);
                if (btn_accept != null)
                {
                    btn_accept.Submit();
                }

                /*Регіон де пробую прочитати скільки часу ще осталось до безкоштовних койнтів*/
                string Stime = "";
                string[] TimeArray = null;
                while (true)
                {
                    IWebElement timeToCkekCoint = Helper.WFETA.FindElementBySomeTime(webDriver, By.CssSelector("#chat > div.condition-container > div.header > a.btn-info-box.free-coins-switcher.btn-check-conditions"), 20);
                    if (timeToCkekCoint != null)
                    {
                        if (!timeToCkekCoint.Text.ToLower().Contains("free"))
                        {
                            Stime = timeToCkekCoint.Text;
                            TimeArray = Stime.Split(':');
                            if (TimeArray[1] != "00" && TimeArray[2] != "00")
                            {
                                Thread.Sleep(1000);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                    Helper.Helper.WriteLog("Таймер " + TimeArray[1] + ":" + TimeArray[2]);
                    if (Convert.ToInt32(TimeArray[1]) > 1)
                    { return CsgoatseCodes.NotTimeYet; }
                }

                /**/

                captchaAgain:
                if (countTrysEnterCaptcha <= 0)
                {
                    return CsgoatseCodes.CantCollectCoins; // Can't solve captcha
                }
                IWebElement free_coins_button = Helper.WFETA.FindElementBySomeTime(webDriver, By.CssSelector("a.btn-info-box.free-coins-switcher.btn-check-conditions"), 50);                                                                                        //btn-info-box free-coins-switcher btn-check-conditions
                if (free_coins_button != null)
                {
                    free_coins_button.Click();
                    Helper.Helper.WriteLog("free_coins_button.Click()");

                    IWebElement captcha = Helper.WFETA.FindElementBySomeTime(webDriver, By.CssSelector("#free-coins > div > img"), 50);
                    if (captcha != null)
                    {
                        string captchaImageBase64 = captcha.GetAttribute("src").Replace("data:image/png;base64,", "");
                        Helper.Helper.WriteLog(captchaImageBase64 + " captchaText.SendKeys");

                        IWebElement captchaText = Helper.WFETA.FindElementBySomeTime(webDriver, By.CssSelector("#free-coins > div > input[type=\"text\"]"), 50);
                        if (captchaText != null)
                        {
                            string textCaptcha = ExampleImageToText(captchaImageBase64);
                            Helper.Helper.WriteLog(textCaptcha + " captchaText.SendKeys");
                            captchaText.SendKeys(textCaptcha.ToLower());
                            Thread.Sleep(400);

                            IWebElement button_sendCaptcha = Helper.WFETA.FindElementBySomeTime(webDriver, By.CssSelector("#free-coins > div > div > button"), 50);
                            if (button_sendCaptcha != null)
                            {
                                Helper.Helper.WriteLog("button_sendCaptcha.Click()");
                                button_sendCaptcha.Click();
                                Thread.Sleep(1000);

                                IWebElement free_coins_button2 = Helper.WFETA.FindElementBySomeTime(webDriver, By.CssSelector("a.btn-info-box.free-coins-switcher.btn-check-conditions"), 50);
                                if (free_coins_button2 != null)
                                {
                                    if (free_coins_button2.Text.ToLower().Contains("collect\x20"))
                                    {
                                        countTrysEnterCaptcha--;
                                        Helper.Helper.WriteLog("free_coins_button2 Click");
                                        free_coins_button2.Click();
                                        Thread.Sleep(1000);
                                        goto captchaAgain;
                                    }
                                }

                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LogBox.AppendText(ex.Message);
                return CsgoatseCodes.ChromeError; // Unknown Chrome Error
            }
            return CsgoatseCodes.CoinsCollected; // All good
        }
        #endregion

        #region Додаткові методи


        // Чек чи все нормально з умовами получення коинсив
        void TerminateChrome()
        {
            foreach (var process in Process.GetProcessesByName("chrome"))
            {
                process.Kill();
            }
        }
        bool CheckForConditions(ChromeDriver driver)
        {
            driver.FindElement(By.Id("free-coins-info-link")).Click();
            Thread.Sleep(1000);
            if (Helper.WFETA.IsElementExists(driver, By.CssSelector(".btn-condition.error"), 10))
            {
                return false; // Плохо
            }

            return true; // Гуд
        }

        /// <summary>
        /// Получити з картинки текст
        /// </summary>
        /// <param name="bodyBase64"> картинка в Base64</param>
        /// <returns></returns>
        private string ExampleImageToText(string bodyBase64)
        {
            var api = new ImageToText
            {
                ClientKey = ConfigForm.ClientKey,
                BodyBase64 = bodyBase64
            };

            if (!api.CreateTask())
            {
                Helper.Helper.WriteLog("API v2 send failed. " + api.ErrorMessage);
                // richTextBox1.AppendText("API v2 send failed. " + api.ErrorMessage + "\n");
            }
            else if (!api.WaitForResult())
            {
                Helper.Helper.WriteLog("Could not solve the captcha.");
                // richTextBox1.AppendText("Could not solve the captcha." + "\n");
            }
            //else
            //{
            //    richTextBox1.AppendText("Result: " + api.GetTaskSolution());
            //}
            return api.GetTaskSolution();
        }
        /// <summary>
        /// Записує дані з форми в змінні
        /// </summary>
        private void AplyConfigForm()
        {
            SteamUserList = (List<User>)bindingSource_users.DataSource;
            ConfigForm.ZentMateLogin = ZMLogin.Text;
            ConfigForm.ZentMatePass = ZMPW.Text;
            ConfigForm.ClientKey = textBox_ClientKey.Text;
        }
        /// <summary>
        /// Зберігає дані з змнних в файл
        /// </summary>
        private void SaveConfigForm()
        {
            if (!File.Exists(AppCurrentDirectory + "Config\\UsersList.json"))
            {
                Helper.Helper.WriteJsonTextToFile(SteamUserList, AppCurrentDirectory + "Config\\UsersList.json");
            }
            else if (SteamUserList.Count > 0)
            {
                Helper.Helper.WriteJsonTextToFile(SteamUserList, AppCurrentDirectory + "Config\\UsersList.json");
            }
            if (!File.Exists(AppCurrentDirectory + "Config\\ConfigForm.json"))
            {
                Helper.Helper.WriteJsonTextToFile(ConfigForm, AppCurrentDirectory + "Config\\ConfigForm.json");
            }
            else if (ConfigForm.ClientKey != null && ConfigForm.ZentMateLogin != null)
            {
                Helper.Helper.WriteJsonTextToFile(ConfigForm, AppCurrentDirectory + "Config\\ConfigForm.json");
            }
        }
        private void ReadConfigForm()
        {
            SteamUserList = Helper.Helper.ReadUsersFromJsFile(AppCurrentDirectory + "Config\\UsersList.json");
            ConfigForm = Helper.Helper.ReadConfigFormFromJsFile("Config\\ConfigForm.json");
        }
        /// <summary>
        /// Загружає данні з змінних на форму
        /// </summary>
        private void LoadConfigForm()
        {
            ZMLogin.Text = ConfigForm.ZentMateLogin;
            ZMPW.Text = ConfigForm.ZentMatePass;
            textBox_ClientKey.Text = ConfigForm.ClientKey;
            bindingSource_users.DataSource = SteamUserList;
            dataGridView_UsersSteam.DataSource = bindingSource_users;
        }
        #endregion

        #region Події і Нажатя кнопок
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            ConfigForm = new ConfigForm();
            SteamUserList = new List<User>();
            SaveConfigForm();
            ReadConfigForm();
            LoadConfigForm();
            AplyConfigForm();
        }
        private void button_Start_Click(object sender, EventArgs e)
        {
            foreach (var user in SteamUserList)
            {

                int Errors = 0;
                InitChrome:
                IWebDriver chromeDriver;
                chromeDriver = null;
                if (Errors > 5) // zamenit whilom)
                {
                    goto End;
                }
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.AddArgument("--mute-audio");
                chromeOptions.AddArgument("--start-maximized");
                chromeOptions.AddArguments("--user-data-dir=" + AppCurrentDirectory +@"Profiles\"+ user.UserLogin);
                try
                {
                    chromeDriver = new ChromeDriver(chromeOptions);
                    SteamLogin:
                    if (Errors > 5) // zamenit whilom)
                    {
                        goto End;
                    }
                    switch (this.SteamLogin(chromeDriver, user.UserLogin, user.UserPass, user.SekretKey))
                    {
                        case SteamLoginCodes.FailedToLogin:
                            Helper.Helper.WriteLog(user.UserLogin + "Failed to login.\n");
                            Errors++;
                            goto SteamLogin;
                        case SteamLoginCodes.LoggedInSteam:
                            Helper.Helper.WriteLog(user.UserLogin + "Logged in Steam.\n");
                            break;
                        case SteamLoginCodes.ChromeNonReach:
                            Helper.Helper.WriteLog(user.UserLogin + "Chrome can't be reached!\n");
                            Errors++;
                            chromeDriver.Quit();
                            TerminateChrome();
                            goto InitChrome;
                        case SteamLoginCodes.ChromeError:
                            Helper.Helper.WriteLog(user.UserLogin + "Chrome Error.\n");
                            Errors++;
                            chromeDriver.Quit();
                            TerminateChrome();
                            goto InitChrome;
                        case SteamLoginCodes.AlreadyLogged:
                            Helper.Helper.WriteLog(user.UserLogin + "Already Logged in.\n");
                            break;
                    }
                    CsgoAtse:
                    if (Errors > 5)
                        goto End;
                    switch (this.CsgoAtse(chromeDriver, user.UserLogin))
                    {
                        case CsgoatseCodes.CoinsCollected:
                            Helper.Helper.WriteLog(user.UserLogin + "Coins collected.\n");
                            break;
                        case CsgoatseCodes.CantCollectCoins:
                            Helper.Helper.WriteLog(user.UserLogin + "Can't collect Coins?!\n");
                            break;
                        case CsgoatseCodes.ChromeError:
                            Helper.Helper.WriteLog(user.UserLogin + "Chrome Error.\n");
                            Errors++;
                            chromeDriver.Quit();
                            TerminateChrome();
                            goto InitChrome;
                        case CsgoatseCodes.NotTimeYet:
                            Helper.Helper.WriteLog(user.UserLogin + "NOT TIME YET.\n");

                            break;

                    }
                }
                catch (Exception)
                {
                    LogBox.AppendText(user.UserLogin + "Unknown error?\n");
                }
                End:
                if (Errors > 5)
                {
                    Helper.Helper.WriteLog(user.UserLogin + "Too many errors, aborting\n");
                }
                chromeDriver.Quit();
                continue;
                
                //int statusSteamLogin = this.SteamLogin(chromeDriver, user.UserLogin, user.UserPass, user.SekretKey);
                //if (statusSteamLogin)
                //{
                //    bool statusCsgoAtse = CsgoAtse(chromeDriver, user.UserLogin);
                //}
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            AplyConfigForm();
            if (String.IsNullOrWhiteSpace(ConfigForm.ZentMateLogin) || String.IsNullOrWhiteSpace(ConfigForm.ZentMatePass))
            {
                MessageBox.Show("Нема введеного пароля або логину Зенмейту");
                return;
            }
            WorkWithProfiles workWithProfiles = new WorkWithProfiles();
            workWithProfiles.Work(SteamUserList, ConfigForm.ZentMateLogin, ConfigForm.ZentMatePass);

        }
        private void dataGridView_UsersSteam_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            AplyConfigForm();
            SaveConfigForm();
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            AplyConfigForm();
            SaveConfigForm();
        }
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            AplyConfigForm();
            SaveConfigForm();
        }

        #endregion
    }
}
