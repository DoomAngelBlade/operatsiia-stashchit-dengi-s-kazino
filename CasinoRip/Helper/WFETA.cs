﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CasinoRip.Helper
{
    public class WFETA
    {

        /// <summary>
        /// Метод який чекає поки появиться елемент
        /// </summary>
        /// <param name="iWebDriver">Змінна веб драйвера</param>
        /// <param name="by">По чому шукати елемент</param>
        /// <param name="OneTenthOfSecond">Скільки часу ждати елемент 10 = 1 Секунді</param>
        /// <returns>true нашло елемент, false не нашло</returns>
        public static IWebElement FindElementBySomeTime(IWebDriver webDriver, By by, int time)
        {
            IWebElement webElement = null;
            if (IsElementExists(webDriver, by, time))
            {
                webElement = webDriver.FindElement(by);
                return webElement;
            };
            return webElement;
        }

        /// <summary>
        /// Метод який чекає поки появиться елемент
        /// </summary>
        /// <param name="iWebDriver">Змінна веб драйвера</param>
        /// <param name="by">По чому шукати елемент</param>
        /// <param name="OneTenthOfSecond">Скільки часу ждати елемент 10 = 1 Секунді</param>
        /// <returns>true нашло елемент, false не нашло</returns>
        public static bool IsElementExists(IWebDriver iWebDriver, By by, int OneTenthOfSecond)
        {
            int Time = 0;
            while (Time < OneTenthOfSecond)
            {
                try
                {
                    if (iWebDriver.FindElement(by).Displayed == true)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                }
                Thread.Sleep(100);
                Time++;
            }
            return false;
        }

    }
}
