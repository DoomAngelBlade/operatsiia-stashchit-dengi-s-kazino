﻿using CasinoRip.Models;
using Newtonsoft.Json;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CasinoRip.Helper
{
    class Helper
    {
        private static object Locker = new object();
        public static string GenerateSteamGuardCodeForTime(long time, string SharedSecret)
        {
            byte[] steamGuardCodeTranslations = new byte[] { 50, 51, 52, 53, 54, 55, 56, 57, 66, 67, 68, 70, 71, 72, 74, 75, 77, 78, 80, 81, 82, 84, 86, 87, 88, 89 };
            if (SharedSecret == null || SharedSecret.Length == 0)
            {
                return "";
            }
            string sharedSecretUnescaped = Regex.Unescape(SharedSecret);
            byte[] sharedSecretArray = Convert.FromBase64String(sharedSecretUnescaped);
            byte[] timeArray = new byte[8];
            time /= 30L;
            for (int i = 8; i > 0; i--)
            {
                timeArray[i - 1] = (byte)time;
                time >>= 8;
            }
            HMACSHA1 hmacGenerator = new HMACSHA1()
            {
                Key = sharedSecretArray
            };
            byte[] hashedData = hmacGenerator.ComputeHash(timeArray);
            byte[] codeArray = new byte[5];
            try
            {
                byte b = (byte)(hashedData[19] & 0xF);
                int codePoint = (hashedData[b] & 0x7F) << 24 | (hashedData[b + 1] & 0xFF) << 16 | (hashedData[b + 2] & 0xFF) << 8 | (hashedData[b + 3] & 0xFF);

                for (int i = 0; i < 5; ++i)
                {
                    codeArray[i] = steamGuardCodeTranslations[codePoint % steamGuardCodeTranslations.Length];
                    codePoint /= steamGuardCodeTranslations.Length;
                }
            }
            catch (Exception)
            {
                return null; //Change later, catch-alls are bad!
            }
            return Encoding.UTF8.GetString(codeArray);
        }
        public static Image Base64ToImage(string base64String)
        {
            // Convert base 64 string to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            // Convert byte[] to Image
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }
        public static void WriteLog(string textError, object LogBox = null)
        {
            lock (Locker)
            {
                if (LogBox != null)
                {

                    RichTextBox RichTextBox = (RichTextBox)LogBox;
                    RichTextBox.BeginInvoke((MethodInvoker)delegate { RichTextBox.AppendText(textError + "\n"); });


                }
                Console.WriteLine(DateTime.Now + ": " + textError);
                WriteLineToFile(DateTime.Now + ": " + textError, "log.txt");
            }
        }
        public static void WriteTextToFile(string text, string path)
        {
            System.IO.File.WriteAllText(path, text);
        }
        public static void WriteLineToFile(string line, string path)
        {
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(line);
            }
        }
        public static string ReadTextFromFile(string path)
        {
            string text = "";
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            using (StreamReader sr = new StreamReader(fs))
            {
                text = sr.ReadToEnd();
            }
            return text;
        }
        public static void WriteJsonTextToFile(object jsonObject, string path)
        {
            JsonSerializer serializer = new JsonSerializer();
            using (StreamWriter sw = new StreamWriter(path))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, jsonObject);
            }
        }
        public static List<User> ReadUsersFromJsFile(string path)
        {
            List<User> obj = null;
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            using (StreamReader sr = new StreamReader(fs))
            {
                obj = JsonConvert.DeserializeObject<List<User>>(sr.ReadToEnd());
            }
            return obj;
        }
        public static ConfigForm ReadConfigFormFromJsFile(string path)
        {
            ConfigForm obj = null;
            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            using (StreamReader sr = new StreamReader(fs))
            {
                obj = JsonConvert.DeserializeObject<ConfigForm>(sr.ReadToEnd());
            }
            return obj;
        }
        public static void CopyPasteShit(string profileName, string ProfileId)
        {
            string AppCurrentDirectory = Directory.GetCurrentDirectory() + "\\";
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(AppCurrentDirectory + @"\NewProfile" + ProfileId + @"\", "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(AppCurrentDirectory + @"\NewProfile" + ProfileId + @"\", AppCurrentDirectory + @"\Profiles\" + profileName + @"\"));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(AppCurrentDirectory + @"\NewProfile" + ProfileId + @"\", "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(AppCurrentDirectory + @"\NewProfile" + ProfileId + @"\", AppCurrentDirectory + @"\Profiles\" + profileName + @"\"), true);
        }
        public static void DeleteProfiles()
        {
            int i = 0;
            TryAgain:
            try
            {
                if (Directory.Exists("Profiles"))
                {
                    Directory.Delete("Profiles", true);
                }
            }
            catch (Exception ex)
            {
                i++;
                Helper.WriteLog(ex.Message);
                if (i > 3)
                {
                    Thread.Sleep(1000);
                    MessageBox.Show("FAILED TO COPY PASTE");

                }
                goto TryAgain;

            }
        }

        public static void ChangeZenmateProfile(IWebDriver driver, string Location)
        {
            driver.Navigate().GoToUrl("chrome-extension://fdcgdnkidjaadafnichfpabhfomcebme/popup.html#locations");
            if (!WFETA.IsElementExists(driver, By.Id("locations"), 50))
            {
                driver.Quit();
                DeleteProfiles();
                MessageBox.Show("НЕ ЗАЛОГИНЕНИЙ!!");
                return;
            }
            if (!WFETA.IsElementExists(driver, By.ClassName("premium"), 35))

            {
                List<IWebElement> locations = driver.FindElements(By.ClassName("location")).ToList();
                foreach (IWebElement location in locations)
                {
                    if (location.Text == Location)
                    {
                        location.Click();
                        break;
                    }
                }

            }
            else
            {
                driver.Quit();
                DeleteProfiles();
                MessageBox.Show("НЕ ПРЕМИУМ!");
                return;
            }
        }
    }
}
