﻿namespace CasinoRip
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Start = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView_UsersSteam = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ZMPW = new System.Windows.Forms.TextBox();
            this.ZMLogin = new System.Windows.Forms.TextBox();
            this.textBox_ClientKey = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.LogBox = new System.Windows.Forms.RichTextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UsersSteam)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_Start
            // 
            this.button_Start.Location = new System.Drawing.Point(1174, 388);
            this.button_Start.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(92, 24);
            this.button_Start.TabIndex = 0;
            this.button_Start.Text = "Start";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(9, 10);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(889, 357);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView_UsersSteam);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Size = new System.Drawing.Size(881, 331);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Юзери";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView_UsersSteam
            // 
            this.dataGridView_UsersSteam.AllowDrop = true;
            this.dataGridView_UsersSteam.AllowUserToOrderColumns = true;
            this.dataGridView_UsersSteam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_UsersSteam.Location = new System.Drawing.Point(14, 13);
            this.dataGridView_UsersSteam.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView_UsersSteam.Name = "dataGridView_UsersSteam";
            this.dataGridView_UsersSteam.RowTemplate.Height = 24;
            this.dataGridView_UsersSteam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_UsersSteam.Size = new System.Drawing.Size(864, 315);
            this.dataGridView_UsersSteam.TabIndex = 9;
            this.dataGridView_UsersSteam.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_UsersSteam_CellValueChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.ZMPW);
            this.tabPage2.Controls.Add(this.ZMLogin);
            this.tabPage2.Controls.Add(this.textBox_ClientKey);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Size = new System.Drawing.Size(881, 331);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Настройки";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Пароль ZenMate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Логин ZenMate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Ключ Антикапчи";
            // 
            // ZMPW
            // 
            this.ZMPW.Location = new System.Drawing.Point(110, 90);
            this.ZMPW.Name = "ZMPW";
            this.ZMPW.Size = new System.Drawing.Size(245, 20);
            this.ZMPW.TabIndex = 11;
            // 
            // ZMLogin
            // 
            this.ZMLogin.Location = new System.Drawing.Point(110, 63);
            this.ZMLogin.Name = "ZMLogin";
            this.ZMLogin.Size = new System.Drawing.Size(245, 20);
            this.ZMLogin.TabIndex = 10;
            // 
            // textBox_ClientKey
            // 
            this.textBox_ClientKey.Location = new System.Drawing.Point(111, 20);
            this.textBox_ClientKey.Name = "textBox_ClientKey";
            this.textBox_ClientKey.Size = new System.Drawing.Size(241, 20);
            this.textBox_ClientKey.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(110, 116);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(245, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Создати профили";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // LogBox
            // 
            this.LogBox.Location = new System.Drawing.Point(903, 32);
            this.LogBox.Name = "LogBox";
            this.LogBox.Size = new System.Drawing.Size(362, 335);
            this.LogBox.TabIndex = 10;
            this.LogBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1277, 423);
            this.Controls.Add(this.LogBox);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button_Start);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "CasinoRip";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_UsersSteam)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView_UsersSteam;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ZMPW;
        private System.Windows.Forms.TextBox ZMLogin;
        private System.Windows.Forms.TextBox textBox_ClientKey;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox LogBox;
    }
}

